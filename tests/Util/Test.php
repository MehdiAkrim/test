<?php
namespace App\Tests\Util;

use App\Entity\Item;
use App\Entity\ToDo;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class Test extends TestCase
{
    public function testUserisValid()
    {
        $user = new User(1,'mehdi','akrim','1998-09-22','mehdi-etude@hotmail.com','abcdrzeaef');
        $this->assertEquals(1,$user->isValid(),'User is not valid');
    }
    public function testToDoListHaveLessThenTenItems() {
        $user = new User(1,'mehdi','akrim','1998-09-22','mehdi-etude@hotmail.com','abcdrzeaef');
        $ToDo= new ToDo(1,$user);
        $item1= new Item(1,'item1',$ToDo,new \DateTime('today'));
        $item2= new Item(2,'item2',$ToDo,new \DateTime('today'));
        $item3= new Item(3,'item3',$ToDo,new \DateTime('today'));
        $item4= new Item(4,'item4',$ToDo,new \DateTime('today'));
        $item5= new Item(5,'item5',$ToDo,new \DateTime('today'));
        $item6= new Item(6,'item6',$ToDo,new \DateTime('today'));
        $item7= new Item(7,'item7',$ToDo,new \DateTime('today'));
        $item8= new Item(8,'item8',$ToDo,new \DateTime('today'));
        $item9= new Item(9,'item9',$ToDo,new \DateTime('today'));
        $item10= new Item(10,'item10',$ToDo,new \DateTime('today'));
        $ToDo->addItem($item1);
        $ToDo->addItem($item2);
        $ToDo->addItem($item3);
        $ToDo->addItem($item4);
        $ToDo->addItem($item5);
        $ToDo->addItem($item6);
        $ToDo->addItem($item7);
        $ToDo->addItem($item8);
        $ToDo->addItem($item9);
        $ToDo->addItem($item10);
        //add one item to crash the error
        $user->setToDo($ToDo);
        $this->assertEquals(1,$user->getToDo()->HaveLessThenTenItems(),'User have more then 10 items');
    }
}