<?php

namespace App\Notification;

use App\Entity\User;
use Twig\Environment;

//npm install -g maildev # Utilisez sudo si nécessaire
//maildev pour demarrer 
// MAILER_URL=smtp://localhost:1025 changer dans le .env pour maildev

class ContactNotification{

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var Environment
     */
    private $renderer;

    public function __construct(\Swift_Mailer $mailer, Environment $renderer){

        $this->mailer = $mailer;
        $this->renderer = $renderer;

    }

    public function send(User $user){
        if($user->isMajor()) {
            $message = ( new \Swift_Message('Item ajouté' .$user->getFullName()))
                ->setFrom('noreply@test.fr')
                ->setTo($user->getEmail())
                ->setBody($this->renderer->render('emails/contact.html.twig',[
                    'user' => $user
                ]), 'text/html');
            $this->mailer->send($message);
            return true;
        }
        else {
            return false;
        }

    }

}