<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $lastName;

    /**
     * @ORM\Column(type="datetime")
     */
    private $birthday;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ToDo", mappedBy="User", cascade={"persist", "remove"})
     */
    private $toDo;

    /**
     * User constructor.
     * @param $id
     * @param $firstName
     * @param $lastName
     * @param $birthday
     * @param $email
     * @param $password
     */
    public function __construct($id, $firstName, $lastName, $birthday, $email, $password)
    {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->birthday = $birthday;
        $this->email = $email;
        $this->password = $password;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }
    public function getFullName() {
        return $this->firstName .' '. $this->lastName;
    }

    public function isMajor() {
        $birthdate = new \DateTime($this->birthday);
        $today   = new \DateTime('today');
        $age = $birthdate->diff($today)->y;
        if($age>=18) {return true;}
        else{
            return false;
        }
    }
    public function isValid() {
        $today=new \DateTime('today');

        $birthdate = new \DateTime($this->birthday);
        $today   = new \DateTime('today');
        $age = $birthdate->diff($today)->y;
        if( ( is_string($this->firstName) && !preg_match('/[#$%^&*()+=\-\[\]\';,.\/{}|":<>?~\\\\]/', $this->firstName) )&&
            ( is_string($this->lastName) && !preg_match('/[#$%^&*()+=\-\[\]\';,.\/{}|":<>?~\\\\]/', $this->lastName))&&
            ( !is_null($this->firstName)) &&
            ( !is_null($this->lastName)) &&
            ( $this->lastName != "") &&
            ( $this->firstName != "") &&

            // date must be in form (Y-M-D)
            ( $age >= 13) &&
            (strpos($this->email, '@') !== false) &&
            (strlen($this->password)>=8)&&
            (strlen($this->password)<=40)
        ) {
            return true;
        } else {
            return false;
        }
    }

    public function getToDo(): ?ToDo
    {
        return $this->toDo;
    }

    public function setToDo(ToDo $toDo): self
    {
        $this->toDo = $toDo;

        // set the owning side of the relation if necessary
        if ($toDo->getUser() !== $this) {
            $toDo->setUser($this);
        }

        return $this;
    }
}
