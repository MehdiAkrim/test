<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ItemRepository")
 */
class Item
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ToDo", inversedBy="items")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ToDo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $date;

    /**
     * Item constructor.
     * @param $id
     * @param $name
     * @param $ToDo
     * @param $date
     */
    public function __construct($id, $name, $ToDo, $date)
    {
        $this->id = $id;
        $this->name = $name;
        $this->ToDo = $ToDo;
        $this->date = $date;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getToDo(): ?ToDo
    {
        return $this->ToDo;
    }

    public function setToDo(?ToDo $ToDo): self
    {
        $this->ToDo = $ToDo;

        return $this;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(string $date): self
    {
        $this->date = $date;

        return $this;
    }
}
