<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ToDoRepository")
 */
class ToDo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="toDo", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $User;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Item", mappedBy="ToDo", orphanRemoval=true)
     */
    private $items;

    /**
     * ToDo constructor.
     * @param $id
     * @param $User
     * @param $items
     */
    public function __construct($id, $User)
    {
        $this->id = $id;
        $this->User = $User;
        $this->items = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(User $User): self
    {
        $this->User = $User;

        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Item $item): self
    {
        if (!$this->items->contains($item)/* && $this->items->last()->getDate()->diff(today)->m<=30*/) {
            $this->items[] = $item;
            $item->setToDo($this);
        }

        return $this;
    }

    public function removeItem(Item $item): self
    {
        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
            // set the owning side to null (unless already changed)
            if ($item->getToDo() === $this) {
                $item->setToDo(null);
            }
        }

        return $this;
    }
    public function HaveLessThenTenItems() {
        if(count($this->items)<=10) {
            return true;
        }
        else {
            return false;
        }
    }
}
